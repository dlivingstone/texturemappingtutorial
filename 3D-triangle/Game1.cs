﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TextureTutorial
{
    /// <summary>
    /// This is a simple texture mapping tutorial for MonoGame
    /// Hello Fraser!
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        // Vectors and Matrices
        Vector3 origin;
        Matrix worldMatrix;
        Matrix projectionMatrix;
        Matrix viewMatrix;

        // Camera
        Vector3 cameraLookAt;
        Vector3 cameraPosition;

        // BasicEffect shader
        BasicEffect basicEffect;

        // Trianglestrip and buffer
        VertexPositionColor[] vertices;
        VertexBuffer vertexBuffer;

        // Textured trianglestrip and buffer
        VertexPositionColorTexture[] texVertices;
        VertexBuffer texVertexBuffer;

        Texture2D sampleTex;

        float texMin;
        float texMax;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            origin = new Vector3(0f,0f,0f);
            worldMatrix = Matrix.CreateWorld(origin, Vector3.Forward, Vector3.Up);
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.ToRadians(60f),
                GraphicsDevice.DisplayMode.AspectRatio, 1f, 500f);
            cameraPosition = new Vector3(0f, 0f, 5f);
            cameraLookAt = new Vector3(0f, 0f, 0f);
            viewMatrix = Matrix.CreateLookAt(cameraPosition,
                cameraLookAt, Vector3.Up);

            basicEffect = new BasicEffect(GraphicsDevice);
            basicEffect.VertexColorEnabled = true;
            basicEffect.LightingEnabled = false;
            basicEffect.Alpha = 1f;

            vertices = new VertexPositionColor[4];
            vertices[0] = new VertexPositionColor(new Vector3(-2, 2, 0),
                Color.Red);
            vertices[1] = new VertexPositionColor(new Vector3(2, 2, 0),
                Color.Green);
            vertices[2] = new VertexPositionColor(new Vector3(-2, -2, 0),
                Color.Blue); 
            vertices[3] = new VertexPositionColor(new Vector3(2, -2, 0),
                Color.White);
            vertexBuffer = new VertexBuffer(GraphicsDevice,
                typeof(VertexPositionColor), 4,
                BufferUsage.WriteOnly);
            vertexBuffer.SetData<VertexPositionColor>(vertices);

            texMin = 0f;
            texMax = 1f;
            texVertices = new VertexPositionColorTexture[4];
            texVertices[0] = new VertexPositionColorTexture(new Vector3(-2, 2, 0), Color.White, new Vector2(texMin, texMin));
            texVertices[1] = new VertexPositionColorTexture(new Vector3(2, 2, 0), Color.Red, new Vector2(texMax, texMin));
            texVertices[2] = new VertexPositionColorTexture(new Vector3(-2, -2, 0), Color.Green, new Vector2(texMin, texMax));
            texVertices[3] = new VertexPositionColorTexture(new Vector3(2, -2, 0), Color.Blue, new Vector2(texMax, texMax));
            texVertexBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColorTexture), 4, BufferUsage.WriteOnly);
            texVertexBuffer.SetData<VertexPositionColorTexture>(texVertices);


            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            sampleTex = Content.Load<Texture2D>("PhoneBox");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            //Matrix rotationX = Matrix.CreateRotationX(
            //    MathHelper.ToRadians(0.5f));
            //Matrix rotationY = Matrix.CreateRotationY(
            //    MathHelper.ToRadians(0.5f));
            //cameraPosition = Vector3.Transform(
            //    cameraPosition, rotationX);
            //cameraPosition = Vector3.Transform(
            //    cameraPosition, rotationY);
            //viewMatrix = Matrix.CreateLookAt(
            //    cameraPosition,cameraLookAt, Vector3.Up);
            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.D1))
                GraphicsDevice.SamplerStates[0] = SamplerState.AnisotropicWrap;
                //GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            if (keyState.IsKeyDown(Keys.D2))
                GraphicsDevice.SamplerStates[0] = SamplerState.AnisotropicClamp;
            if (keyState.IsKeyDown(Keys.D3))
                GraphicsDevice.SamplerStates[0] = SamplerState.PointWrap;
            //GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
            if (keyState.IsKeyDown(Keys.D4))
                GraphicsDevice.SamplerStates[0] = SamplerState.PointClamp;


            bool texUVChanged = false;

            if (keyState.IsKeyDown(Keys.Left))
            {
                texMin -= 0.05f;
                texUVChanged = true;
            }
            if (keyState.IsKeyDown(Keys.Right))
            {
                texMin += 0.05f;
                texUVChanged = true;
            }
            if (keyState.IsKeyDown(Keys.Up))
            {
                texMax += 0.05f;
                texUVChanged = true;
            }
            if (keyState.IsKeyDown(Keys.Down))
            {
                texMax -= 0.05f;
                texUVChanged = true;
            }

            if (texUVChanged)
            {
                texVertices = new VertexPositionColorTexture[4];
                texVertices[0] = new VertexPositionColorTexture(new Vector3(-2, 2, 0), Color.White, new Vector2(texMin, texMin));
                texVertices[1] = new VertexPositionColorTexture(new Vector3(2, 2, 0), Color.White, new Vector2(texMax, texMin));
                texVertices[2] = new VertexPositionColorTexture(new Vector3(-2, -2, 0), Color.White, new Vector2(texMin, texMax));
                texVertices[3] = new VertexPositionColorTexture(new Vector3(2, -2, 0), Color.White, new Vector2(texMax, texMax));
                texVertexBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionColorTexture), 4, BufferUsage.WriteOnly);
                texVertexBuffer.SetData<VertexPositionColorTexture>(texVertices);
            }



            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            basicEffect.World = worldMatrix;
            basicEffect.Projection = projectionMatrix;
            basicEffect.View = viewMatrix;
            basicEffect.Texture = sampleTex;
            basicEffect.TextureEnabled = true; 

            //GraphicsDevice.SetVertexBuffer(vertexBuffer);
            GraphicsDevice.SetVertexBuffer(texVertexBuffer);
            GraphicsDevice.VertexTextures[0] = sampleTex;

            //Turn off culling so we see both sides of our rendered triangle
            RasterizerState rasterizerState = new RasterizerState();
            rasterizerState.CullMode = CullMode.CullCounterClockwiseFace;
            GraphicsDevice.RasterizerState = rasterizerState;

            foreach(EffectPass pass in 
                basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                GraphicsDevice.Textures[0] = sampleTex;
                basicEffect.Texture = sampleTex;
                GraphicsDevice.DrawPrimitives(PrimitiveType.TriangleStrip, 0, 2);

            }

            base.Draw(gameTime);
        }
    }
}
